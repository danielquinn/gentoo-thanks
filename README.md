gentoo-thanks
=============

A proposal for a way to thank Gentoo developers

I <3 Gentoo Linux, but I'm not blessed with the patience and skill required to
stay on top of things like package releases and bug reports.  Regardless, I've
been a devoted Gentoo user since 2000 and I've had few opportunities to give
back to all of the fabulous people that make Gentoo awesome.

Part of the reason behind this is that *I don't know who to thank* really.
Sure, I can donate to the foundation itself (and I have) but I wanted to know
where and how I might show my gratitude to the developers who do the stuff I
love, and when I asked around [the answers were less-than-clear](https://plus.google.com/105563703093466990245/posts/XNi6GoDbV9t).

So I made this simple one-page app: a way for users to find and thank their
favourite developers in all sorts of ways.  You can:

* Drop them a tweet
* Visit their website
* Fork their GitHub
* Send them some cash via Gittip
* Buy them some stuff from their amazon wish lists
* There's even [bitcoin](https://bitcoin.org/en/) support for those who dig that sort of thing

Presently, all I have is a [working demo](http://danielquinn.github.io/projects/gentoo-thanks/)
in which **all of the support data is mocked**.  It's just meant as a way to
show the Gentooers at FOSDEM what we can do if someone wants to put this up, so
**don't donate anything** just yet.  If the web team accepts this idea, it'll
end up on the actual site (in one form or another) and then you can show as much
love as you like :-)

## How It's Made

This is really just a glomming together of a bunch of super-handy front-end
tools and frameworks:

### Frontend

* [The Gentoo theme](https://github.com/gentoo/tyrian) based on [Twitter Bootstrap](https://getbootstrap.com/) is used to make things sexy.
* [Isotope](http://isotope.metafizzy.co/) manages the fancy boxes flying around and the filtration.
* [Font Awesome](http://fontawesome.io/) is responsible for the nice icons everywhere.
* [Bitcoin-donate](https://github.com/danielquinn/bitcoin-donate) is a separate project I developed to make bitcoin donations easier.  It depends on the [jquery.qrcode library](https://github.com/lrsjng/jQuery.qrcode).
* And everything is built on top of [jQuery](http://jquery.com/).

### "Backend"

There's not much of a backend at present, just a Python script that parses the
[current developers page on Gentoo.org](http://www.gentoo.org/proj/en/devrel/roll-call/userinfo.xml)
and goes about rewriting the HTML to make it more donations-friendly (and
well... prettier).  Ideally, this would be served via a proper database, or even
just a maintained `.json` file, but I leave that up to whomever might adopt this
for gentoo.org.
