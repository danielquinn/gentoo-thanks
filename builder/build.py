#!/usr/bin/env python3

#
# Obviously, this is not the best way to compile the file, but given that I
# don't have access to an actual database, I thought this would be alright for
# now.
#

import json

from bs4 import BeautifulSoup
from re import sub
from random import randint
from urllib.request import urlopen

class Thanks(object):

    URL = "http://www.gentoo.org/proj/en/devrel/roll-call/userinfo.xml"

    def __init__(self):

        self.soup = BeautifulSoup(urlopen(self.URL).read())
        self.developers = []
        self.responsibilities = {}


    def generate(self):

        rows = self.soup.select("table.ntable")[0].select("tr")[1:]

        developers = ""
        while rows:
            row = rows.pop(randint(0, len(rows) - 1))  # Pop out a random row
            developers += self._generate_row_html(row)

        r = ""
        with open("template.html") as template:
            r = template.read()

        print(
            r.replace(
                "{{ collection }}",
                developers
            ).replace(
                "{{ responsibilities }}",
                json.dumps(self.responsibilities)
            ).replace(
                "{{ developers }}",
                json.dumps(self.developers)
            )
        )


    def _generate_row_html(self, row):

        columns = row.select("td")

        username         = columns[0].get_text()
        name             = columns[1].get_text()
        location         = columns[3].get_text().replace("\n", "")
        keys             = ""
        responsibilities = ""

        self.developers.append(username)

        keys_list = [sub("<[^>]*>", "", str(key)) for key in columns[2].contents if key]
        responsibilities_list = [r.strip() for r in columns[4].get_text().split(",")]

        if keys_list:
            keys = self.make_ul(keys_list, cls="primary")
        if responsibilities_list:
            responsibilities = self.make_ul(responsibilities_list)
            for responsibility in responsibilities_list:
                if responsibility not in self.responsibilities:
                    self.responsibilities[responsibility] = []
                self.responsibilities[responsibility].append(username)

        twitter = ""
        if not randint(0, 2):
            twitter = '<a href="https://twitter.com/{username}/" class="btn btn-info btn-sm twitter" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a>'.format(
                username=username
            )

        github = ""
        if not randint(0, 1):
            github = '<a href="https://github.com/{username}" class="btn btn-primary btn-sm github" data-toggle="tooltip" title="GitHub"><i class="fa fa-github"></i></a>'.format(
                username=username
            )

        gittip = ""
        if not randint(0, 3):
            gittip = '<a href="https://gratipay.com/{username}/" class="btn btn-primary btn-sm gittip" data-toggle="tooltip" title="Gittip"><i class="fa fa-gittip"></i></a>'.format(
                username=username
            )

        www = ""
        if not randint(0, 3):
            www = '<a href="#" class="btn btn-primary btn-sm www" data-toggle="tooltip" title="Website"><i class="fa fa-globe"></i></a>'

        amazon = ""
        if not randint(0, 3):
            amazon = '<a href="#" class="btn btn-danger btn-sm amazon" data-toggle="tooltip" title="Amazon Wishlist"><i class="fa fa-gift"></i></a>'

        bitcoin = ""
        if not randint(0, 4):
            bitcoin = '<a href="bitcoin:NOT-A-REAL-BITCOIN-ADDRESS" class="btn btn-warning btn-sm bitcoin"><i class="fa fa-btc"></i></a>'

        html = """
            <div class="developer" data-filters="{username} {responsibility_classes}" id="{username}">
              <div class="facets">
                {github}
                {gittip}
                {twitter}
                {www}
                {amazon}
                {bitcoin}
              </div>
              <h2>
                <span class="username">{username}</span><br />
                <small class="name">{name}</small>
              </h2>
              <div class="location"><i class="fa fa-map-marker"></i> {location}</div>
              <div class="keys">{keys}</div>
              <div class="responsibilities">{responsibilities}</div>
            </div>
        """
        return html.format(
            username=username,
            name=name,
            keys=keys,
            responsibilities=responsibilities,
            location=location,
            github=github,
            gittip=gittip,
            twitter=twitter,
            www=www,
            amazon=amazon,
            bitcoin=bitcoin,
            responsibility_classes=" ".join(responsibilities_list).lower()
        )


    @staticmethod
    def make_ul(items, cls="default"):
        open_tag = '<span class="label label-{cls}">'.format(cls=cls)
        close_tag = '</span>'
        joiner = "{close} {open}".format(open=open_tag, close=close_tag)
        return open_tag + joiner.join(items) + close_tag


if __name__ == "__main__":
    Thanks().generate()
