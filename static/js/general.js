var Filtering = function(){

  var self = this;

  this.$filter = $("#searchInput");
  this.$labels = $(".responsibilities span.label");
  this.$permalink = $("#permalink");

  this.querystring_regex = /^.*\?.*?f=([^#&]+)/;
  this.base_url = window.location.href.replace(/\?.*/, "");

  this.$isotope = $("#developers").isotope({
    itemSelector: '.developer',
    masonry: {}
  });


  this.filter = function(regex){
    self.$isotope.isotope({
      filter: function(){
        return $(this).attr("data-filters").match(regex);
      }
    })
  };


  this._handle_filter = function(){

    var filter = self.$filter.val().toLowerCase();

    var permalink = "";
    if (filter.length) {
      permalink = self.base_url + "?f=" + encodeURIComponent(filter);
      permalink = '<a href="' + permalink + '">' + permalink + '</a>';
    }
    self.$permalink.html(permalink);

    self.filter(new RegExp(filter.replace(" ", "|")));

  };


  this._handle_click = function(){
    self.$filter.val($(this).text());
    self._handle_filter();
  };


  this.setup = function(){

    self.$labels.on("click", self._handle_click);
    self.$filter.on("keyup change", self._handle_filter);

    var match = window.location.href.match(self.querystring_regex);
    if (match && match.length == 2) {
      self.$filter.val(decodeURIComponent(match[1].replace("+", "%20")));
      self._handle_filter();
    } else if (self.$filter.val().length) {
      self._handle_filter();
    }

    return self;

  };

  return this;

};

Filtering().setup();

// Bitcoin Buttons
btcdonate();

// Tooltips
$(function(){
  $('[data-toggle="tooltip"]').tooltip({
    placement: "top"
  })
});
